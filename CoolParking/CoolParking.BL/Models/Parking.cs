﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        internal decimal Balance { get; set; } = Settings.Balance; 
        public int Capacity => Settings.Capacity;
        public int FreePlaces => Capacity - Vehicles.Count;
        public List<Vehicle> Vehicles { get; } = new List<Vehicle>(Settings.Capacity);

        // Lazy realization of Singleton
        private static readonly Lazy<Parking> lazy =
            new Lazy<Parking>(() => new Parking());

        private Parking() {}
        
        public static Parking Instance =>  lazy.Value;        
        public void AddVehicle(Vehicle vehicle)
        {
            if (FreePlaces == 0)
                throw new InvalidOperationException("There is no place, try again later.");
            if (Vehicles.Find(x => x.Id == vehicle.Id) != null)
                throw new ArgumentException("This vehicle is already here.");            
            Vehicles.Add(vehicle);
        }
        public void RemoveVehicle(string vehicleId)
        {            
            Vehicle vehicle = Vehicles.SingleOrDefault(vehicle => vehicle.Id == vehicleId);
            if(vehicle == null)
                throw new ArgumentException("This vehicle isn't here.");
            if (vehicle.Balance < 0)
                throw new InvalidOperationException("You should top up your account in order to leave.");
            Vehicles.Remove(vehicle);                                    
        }
        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum <= 0)
                throw new ArgumentException("TopUp sum should be greater than 0.");
            try
            {
                Vehicles.Single(vehicle => vehicle.Id == vehicleId).TopUp(sum);
            }
            catch (Exception)
            {
                throw new ArgumentException("This vehicle isn't here.");
            }            
        }        
        public List<TransactionInfo> WithdrawPayment()
        {
            List<TransactionInfo> transactions = new List<TransactionInfo>(Vehicles.Count);
            foreach (Vehicle vehicle in Vehicles)
            {
                decimal tariff = GetTariff(vehicle);
                transactions.Add(new TransactionInfo(vehicle.Id, tariff));                
                vehicle.Balance -= tariff;
                Balance += tariff;
            }
            return transactions;
        }
        private decimal GetTariff(Vehicle vehicle)
        {
            decimal tariff = Settings.Tarriffs[vehicle.VehicleType];
            if (vehicle.Balance < tariff)
                tariff = GetPenaltyTariff(tariff, vehicle.Balance);
            return tariff;
        }
        private decimal GetPenaltyTariff(decimal normalTariff, decimal balance)
        {
            return balance <= 0 
                ? normalTariff * Settings.PenaltyRatio 
                : (normalTariff - balance) * Settings.PenaltyRatio + balance;
        }           
    }
}