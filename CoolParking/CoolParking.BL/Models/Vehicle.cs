﻿using Fare;
using System;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        private string id;
        const string idPattern = @"[A-Z]{2}-\d{4}-[A-Z]{2}";
        public decimal Balance { get; internal set; } 
        public VehicleType VehicleType { get; private set; }

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            if (balance < 0)
                throw new ArgumentException("Balance should be greater than zero");
            Balance = balance;
            Id = id;
            VehicleType = vehicleType;
        }
        public string Id
        {
            get => id;
            private set
            {
                if (!Regex.IsMatch(value, idPattern))
                    throw new ArgumentException("Incorrect vehicle id");
                id = value;
            }
        }
        public void TopUp(decimal value)
        {
            if (value <= 0)
                throw new ArgumentException("Value should be greater than zero");
            Balance += value;
        }
        public static string GenerateRandomRegistrationPlateNumber()
        {
            try
            {
                Xeger stringGenerator = new Xeger(idPattern);
                return stringGenerator.Generate();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return string.Empty;
            }
        }
        public override string ToString()
        {
            string type = Enum.GetName(typeof(VehicleType), VehicleType);
            return $"Id: {Id}\nBalance: {Balance}\nType: {type}";
        }
    }
}