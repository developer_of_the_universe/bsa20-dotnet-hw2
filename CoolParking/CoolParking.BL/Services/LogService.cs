﻿using CoolParking.BL.Interfaces;
using System;
using System.IO;
using System.Text;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public string LogPath { get; }        
        public LogService(string logPath)
        {
            LogPath = logPath;
        }
        public string Read()
        {
            try
            {
                using (StreamReader sr = new StreamReader(LogPath))
                {
                    return sr.ReadToEnd();
                }
            }
            catch (Exception)
            {
                throw new InvalidOperationException("Error while reading");
            }            
        }

        public void Write(string logInfo)
        {
            if (string.IsNullOrWhiteSpace(logInfo))
                return;
            try
            {                
                using (StreamWriter sw = new StreamWriter(LogPath, true, Encoding.Default))
                {
                    sw.WriteLine(logInfo);
                }
            }
            catch (Exception)
            {
                throw new InvalidOperationException("Error while writing");
            }            
        }
    }
}