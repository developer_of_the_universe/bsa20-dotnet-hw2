﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        Parking parking = Parking.Instance;
        List<TransactionInfo> transactions = new List<TransactionInfo>();
        ITimerService WithdrawTimer;
        ITimerService LogTimer;
        ILogService Logger;
        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logger)
        {            
            WithdrawTimer = withdrawTimer;            
            LogTimer = logTimer;
            SetUpTimers();
            Logger = logger;
        }
        private void WithdrawPaymentFromVehicles(object sender, ElapsedEventArgs args)
        {            
            transactions.AddRange(parking.WithdrawPayment());            
        }
        private void WriteTransactionsIntoLog(object sender, ElapsedEventArgs args)
        {                                    
            Logger.Write(StringOfTransactions());
            transactions.Clear();
        }
        public void AddVehicle(Vehicle vehicle)
        {
            parking.AddVehicle(vehicle);
        }        
        public decimal GetBalance()
        {
            return parking.Balance;
        }

        public int GetCapacity()
        {
            return parking.Capacity;
        }

        public int GetFreePlaces()
        {
            return parking.FreePlaces;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return transactions.ToArray();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(parking.Vehicles);
        }

        public string ReadFromLog()
        {
            return Logger.Read();
        }
        public void RemoveVehicle(string vehicleId)
        {
            parking.RemoveVehicle(vehicleId);
        }
        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            parking.TopUpVehicle(vehicleId, sum);   
        }
        private void SetUpTimers()
        {
            WithdrawTimer.Interval = Settings.TransactionPeriod;
            WithdrawTimer.Elapsed += WithdrawPaymentFromVehicles;
            WithdrawTimer.Start();
            LogTimer.Interval = Settings.LogPeriod;
            LogTimer.Elapsed += WriteTransactionsIntoLog;
            LogTimer.Start();
        }      
        private string StringOfTransactions()
        {
            StringBuilder sb = new StringBuilder(transactions.Count);
            transactions.ForEach(transaction => sb.Append(transaction.ToString() + "\n"));
            return sb.ToString();
        }        
        public void Dispose()
        {
            WithdrawTimer.Dispose();
            LogTimer.Dispose();
            transactions.Clear();
            parking.Vehicles.Clear();
            parking.Balance = Settings.Balance;
        }
    }
}