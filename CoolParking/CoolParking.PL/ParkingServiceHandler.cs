﻿using CoolParking.BL.Models;
using CoolParking.BL.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CoolParking.PL
{
    static class ParkingServiceHandler
    {
        static ParkingService parkingService;
        static bool GoToMainMenu = false;
        static ParkingServiceHandler()
        {
            LogService logger = new LogService("Transactions.log");
            TimerService logTimer = new TimerService();
            TimerService withdrawTimer = new TimerService();
            parkingService = new ParkingService(withdrawTimer, logTimer, logger);
        }
        internal static string GetBalance()
        {
            return "Balance of Parking is " + parkingService.GetBalance();
        }
        internal static string GetCurrentIncome()
        {
            decimal income = parkingService.GetLastParkingTransactions()
                .Select(tr => tr.Sum).Sum();
            return "Current income of Parking is " + income;
        }
        internal static string GetFreePlacesInfo()
        {
            int free = parkingService.GetFreePlaces();
            int capacity = parkingService.GetCapacity();
            return $"Free {free} places of {capacity}";
        }
        internal static string GetLastTransactions()
        {
            TransactionInfo[] transactions = parkingService.GetLastParkingTransactions();
            if (transactions.Length == 0)
                return "There are no current transactions now";
            string result = string.Join("\n", transactions.Select(tr => tr.ToString()));
            return "List of transactions:\n" + result;
        }
        internal static string ReadFromLog()
        {
            string result = parkingService.ReadFromLog();
            if (string.IsNullOrWhiteSpace(result))
                return "There are no transactions in the log now";
            return "Transactions from log:\n" + result;
        }
        internal static string GetVehicles()
        {
            IReadOnlyCollection<Vehicle> vehicles = parkingService.GetVehicles();
            if (!vehicles.Any())
                return "There are no vehicles yet";
            string separator = "\n============================\n";
            string result = string.Join(separator, vehicles.Select(vehicle => vehicle.ToString()));
            return "All vehicles:\n\n" + result;
        }
        internal static string AddVehicle()
        {            
            if (parkingService.GetFreePlaces() == 0)
                return "There is no place, try again later";
            GoToMainMenu = false;
            const string cancel = "Operation canceled";
            string id = CreateVehicleId();
            if (GoToMainMenu)
                return cancel;
            VehicleType type = GetVehicleType();
            if (GoToMainMenu)
                return cancel;
            decimal balance = GetSumForBalance();
            if (GoToMainMenu)
                return cancel;
            try
            {
                Vehicle vehicle = new Vehicle(id, type, balance);
                parkingService.AddVehicle(vehicle);
                return vehicle.ToString();
            }
            catch (Exception ex)
            {
                return "Failed:\n" + ex.Message;
            }
        }
        internal static string RemoveVehicle()
        {
            GoToMainMenu = false;
            string id = GetVehicleIdFromUser();
            if (GoToMainMenu)
                return "Operation canceled";
            try
            {
                parkingService.RemoveVehicle(id);
                return $"Successfully removed {id}";
            }
            catch (Exception ex)
            {
                return "Failed:\n" + ex.Message;
            }
        }
        internal static string TopUpBalance()
        {
            GoToMainMenu = false;
            string id = GetVehicleIdFromUser();
            if (GoToMainMenu)
                return "Operation canceled";
            decimal sum = GetSumForBalance();
            if (GoToMainMenu)
                return "Operation canceled";
            try
            {
                parkingService.TopUpVehicle(id, sum);
                return $"Topped up {id} on {sum}";
            }
            catch (ArgumentException ex)
            {
                return "Failed:\n" + ex.Message;
            }
        }
        private static string CreateVehicleId()
        {
            string randomCase = "random";
            string goBackCase = "back";
            Console.Clear();
            Console.WriteLine("Enter id of your vehicle to park(in format XX-YYYY-XX)\n"
                + $"Or {randomCase} to generate it\n"
                + $"Or {goBackCase} to go back\n");
            string result = InputHandler.GetStringFromUser();
            if (result == goBackCase)
                GoToMainMenu = true;
            return result != randomCase ? result : Vehicle.GenerateRandomRegistrationPlateNumber();
        }
        private static VehicleType GetVehicleType()
        {
            Console.Clear();
            Console.WriteLine("Choose type of your vehicle:\n"
                + "1. PassengerCar\n"
                + "2. Truck\n"
                + "3. Bus\n"
                + "4. Motorcycle\n"
                + "b. Cancel\n");
            char input = InputHandler.GetCharFromUserByCondition(input => input == 'b' || input >= '1' && input <= '4');
            if (input == 'b')
            {
                GoToMainMenu = true;
                return default;
            }
            return input switch
            {
                '1' => VehicleType.PassengerCar,
                '2' => VehicleType.Truck,
                '3' => VehicleType.Bus,
                '4' => VehicleType.Motorcycle,
                _ => throw new ArgumentException("Impossible value is possible)")
            };
        }
        private static decimal GetSumForBalance()
        {
            Console.Clear();
            Console.WriteLine("Enter sum for adding to your balance (greater than 0) or -1 to Cancel:\n");
            decimal result = InputHandler.GetDecimalFromUserByCondition(input => input == -1 || input > 0);
            if (result == -1)
                GoToMainMenu = true;
            return result;
        }
        private static string GetVehicleIdFromUser()
        {
            string goBackCase = "back";
            Console.Clear();
            Console.WriteLine($"Enter id of vehicle (XX-YYYY-XX) or \"{goBackCase}\" to go back: ");
            string id = InputHandler.GetStringFromUser();
            if (id == goBackCase)
                GoToMainMenu = true;
            return id;
        }        
    }
}
