﻿using System;

namespace CoolParking.PL
{
    class Program
    {        
        static void Main(string[] args)
        {            
            try
            {
                Application.Run();
                Console.WriteLine("Press any key to exit");
                Console.ReadKey();
            }
            catch (Exception)
            {
                Console.WriteLine("Unhandled error");
            }
        }
    }
}
